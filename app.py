from flask import Flask, jsonify, flash, request, redirect
import numpy as np
import keras
import tensorflow as tf
from keras.models import load_model
from random import randint
import skvideo.io
import io
import cv2
import os
import base64
from werkzeug.utils import secure_filename
from PIL import Image

keras.backend.clear_session()
UPLOAD_FOLDER = '/Users/divan/Desktop/webserverlicenta'
ALLOWED_EXTENSIONS = set(['mov', 'avi', 'mp4'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


num_frames = 25

app = Flask(__name__)
global graph
graph = tf.get_default_graph()


global model
model = load_model('tennis_12shot_analysis_fullyConnected_examplele_AdamDropout065.h5')
global calibrate_model
calibrate_model = load_model("tennis_3shot_analysis_fullyConnected_Adam20EpociDropout0875.h5")
global flow_model
flow_model = load_model("tennis_3shot_analysis_fullyConnected_flowAdamDropout083Epoci.h5")


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def center_crop_frame(frame):
    # width and height are selected according to the papaer
    new_width = 224
    new_height = 224

    height = np.size(frame, 0)
    width = np.size(frame, 1)

    left = int(np.ceil((width - new_width) / 2.))
    top = int(np.ceil((height - new_height) / 2.))
    # right = int(np.floor((width + new_width) / 2.))
    right = int(width - np.floor((width - new_width) / 2))
    bottom = int(np.floor((height + new_height) / 2.))
    croppedFrame = frame[top:bottom, left:right]

    # croppedFrame = np.round(croppedFrame).astype(np.int8)
    return croppedFrame


def process_rgb_video(video_capture):
    # we assume the video is in a wide-format (e.g. 16/9) such as the smallest dimension is video_capture.shape[1]
    SMALLEST_DIMENSION = 256
    aspect_ratio = video_capture.shape[2] / video_capture.shape[1]
    height = SMALLEST_DIMENSION
    width = int(aspect_ratio * height)

    resized_video = np.empty((video_capture.shape[0], 224, 224, 3))

    for i in range(0, video_capture.shape[0]):
        frame = video_capture[i]
        resized_frame = cv2.resize(frame, (width, height), interpolation=cv2.INTER_LINEAR)

        minimum_in_frame = np.amin(resized_frame)
        maximum_in_frame = np.amax(resized_frame)
        # change range to (0, 1) to have the videos viewable in a player
        resized_frame = np.interp(resized_frame, (minimum_in_frame, maximum_in_frame), (-1, +1))

        centered_frame = center_crop_frame(resized_frame)
        # optiflow[i - 1] = centered_frame
        resized_video[i] = centered_frame

    return resized_video


def lengthwise_crop(video, video_type):
    if video.shape[0] > num_frames:
        start_frame = randint(0, video.shape[0] - num_frames)
        video = video[start_frame:(start_frame + num_frames), :, :, :]
        return video

    else:
        if video_type == "RGB":
            new_video = np.zeros((num_frames, 224, 224, 3)) #if rgb, change for optiflow
        else:
             new_video = np.zeros((num_frames, 224, 224, 2))

        new_video[0:video.shape[0], :, :, :] = video[:, :, :, :]
        new_video[(video.shape[0] + 1): num_frames, :, :, :] = video[0:(num_frames - video.shape[0])]
        return new_video


def process_optiflow(video_capture):
    SMALLEST_DIMENSION = 256
    aspect_ratio = video_capture.shape[2] / video_capture.shape[1]
    height = SMALLEST_DIMENSION
    width = int(aspect_ratio * height)
    # optiflow = np.empty((video_capture.shape[0], height, width, 2))
    optiflow = np.empty((video_capture.shape[0], 224, 224, 2))

    previous_frame = video_capture[0]
    resized_previous_frame = cv2.resize(previous_frame, (width, height), interpolation=cv2.INTER_LINEAR)
    resized_previous_frame = cv2.cvtColor(resized_previous_frame, cv2.COLOR_BGR2GRAY)
    for i in range(1, video_capture.shape[0]):
        frame = video_capture[i]
        resized_frame = cv2.resize(frame, (width, height), interpolation=cv2.INTER_LINEAR)
        grayscale_frame = cv2.cvtColor(resized_frame, cv2.COLOR_BGR2GRAY)
        optiflowed_frame = compute_TVL1(resized_previous_frame, grayscale_frame)
        resized_previous_frame = grayscale_frame
        centered_frame = center_crop_frame(optiflowed_frame)
        optiflow[i - 1] = centered_frame

        # info = np.iinfo(centered_frame.dtype)  # Get the information of the incoming image type
        data = centered_frame / np.max(centered_frame)  # normalize the data to 0 - 1

        optiflow[i - 1] = data

    return optiflow


def compute_TVL1(prev, curr):
    """Compute the TV-L1 optical flow."""
    TVL1 = cv2.optflow.createOptFlow_DualTVL1()
    flow = TVL1.calc(prev, curr, None)
    assert flow.dtype == np.float32

    flow[flow < -20] = -20
    flow[flow > 20] = 20

    min_flow = np.min(flow)
    max_flow = np.max(flow)
    # change range to (0, 1) to have the videos viewable in a player
    flow = np.interp(flow, (min_flow, max_flow), (0, 1))

    return flow

@app.route('/predict/complex_shot_analyzer', methods=['GET', 'POST'])
def predict_rgb_video():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(UPLOAD_FOLDER, filename)
            file.save(filepath)
            video_capture = skvideo.io.vread(filepath)
            resized_video = process_rgb_video(video_capture)
            cropped_video = lengthwise_crop(resized_video, video_type="RGB")
            with graph.as_default():
                myArray = np.array([cropped_video])
                print(myArray.shape)
                prediction = model.predict(np.array([cropped_video]))

            output = np.argmax(prediction, axis=1)
            model_response = int(output[0])

            category_map = {
                0: "forehand volley",
                1: "forehand slice",
                2: "forehand flat",
                3: "open-stance forehand",
                4: "backhand volley",
                5: "backhand slice",
                6: "one-handed backhand",
                7: "two-handed backhand",
                8: "smash",
                9: "slice serve",
                10: "kick serve",
                11: "flat serve"
            }

            response = {
                "model_response": model_response,
                "model_name": "Shot analyzer",
                "category_map": category_map,
            }
            json = jsonify(response)

            if os.path.exists(filepath):
                os.remove(filepath)

            return json

@app.route('/predict/calibrate_shots', methods=['GET', 'POST'])
def predict_calibrate_shots():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(UPLOAD_FOLDER, filename)
            file.save(filepath)
            video_capture = skvideo.io.vread(filepath)
            resized_video = process_rgb_video(video_capture)
            cropped_video = lengthwise_crop(resized_video, video_type="RGB")
            with graph.as_default():
                myArray = np.array([cropped_video])
                print(myArray.shape)
                prediction = calibrate_model.predict(np.array([cropped_video]))

            output = np.argmax(prediction, axis=1)
            model_response = int(output[0])

            category_map = {
                0: "forehand",
                1: "backhand",
                2: "serve",
            }

            middleImage = cropped_video[int(num_frames / 2), :, :]
            resized_base64_image = cv2.resize(middleImage, dsize=(60, 60), interpolation=cv2.INTER_AREA)
            resized_base64_image = cv2.normalize(resized_base64_image, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)

            with io.BytesIO() as output_bytes:
                PIL_image = Image.fromarray(resized_base64_image)
                PIL_image.save(output_bytes, 'JPEG')
                bytes_data = output_bytes.getvalue()

            # encode bytes to base64 string
            base64_img = str(base64.b64encode(bytes_data), 'utf-8')

            response = {
                "model_response": model_response,
                "model_name": "Calibrate your shots",
                "category_map": category_map,
                "base64Image": base64_img
            }
            json = jsonify(response)

            if os.path.exists(filepath):
                os.remove(filepath)

            return json


@app.route('/predict/calibrate_shots_flow', methods=['GET', 'POST'])
def predict_calibrate_shots_flow():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            filepath = os.path.join(UPLOAD_FOLDER, filename)
            file.save(filepath)
            video_capture = skvideo.io.vread(filepath)
            resized_video = process_optiflow(video_capture)
            cropped_video = lengthwise_crop(resized_video, video_type="flow")
            with graph.as_default():
                myArray = np.array([cropped_video])
                print(myArray.shape)
                prediction = flow_model.predict(np.array([cropped_video]))

            output = np.argmax(prediction, axis=1)
            model_response = int(output[0])

            category_map = {
                0: "forehand",
                1: "backhand",
                2: "serve",
            }

            middleImage = video_capture[int(num_frames / 2), :, :]
            resized_base64_image = cv2.resize(middleImage, dsize=(60, 60), interpolation=cv2.INTER_AREA)
            resized_base64_image = cv2.normalize(resized_base64_image, None, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)

            with io.BytesIO() as output_bytes:
                PIL_image = Image.fromarray(resized_base64_image)
                PIL_image.save(output_bytes, 'JPEG')
                bytes_data = output_bytes.getvalue()

            # encode bytes to base64 string
            base64_img = str(base64.b64encode(bytes_data), 'utf-8')

            response = {
                "model_response": model_response,
                "model_name": "Calibrate your shots with flow",
                "category_map": category_map,
                "base64Image": base64_img
            }
            json = jsonify(response)

            if os.path.exists(filepath):
                os.remove(filepath)

            return json


if __name__ == '__main__':
    app.run()
